package ga.studiocloud.blurdetection;

import android.Manifest;
import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.android.Zoomcameraview;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";
    private TextView mBlur;
    private Zoomcameraview mOpenCvCameraView;
    private Mat mRgba;
    private Mat mRgbaF;
    private Mat mRgbaT;
    private Zoomcameraview mHelloOpenCvView;
    /**
     * BLUR
     */
    private TextView mBlurIndicator;
    private View mShutterButton;
    private ImageView mFlashMode;
    private SeekBar mCameraZoomControls;
    String flashMode = "auto";
    private ImageView mPreviewFoto;
    private RelativeLayout mOverlay;
    private ImageView mUpload;
    private ImageView mRetake;
    private View mTouchfocus;
    private SensorManager sensonManager;
    private ImageView mBackViewer;
    private AsyncHttpClient klien;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        initView();

        OrientationEventListener orientationEventListener = new OrientationEventListener(this) {
            @Override
            public void onOrientationChanged(int i) {
                if (i > 45 && i < 135) {
                    if (!mFlashMode.getTag().equals("90")) {
                        mFlashMode.animate().rotation(-90).start();
                        mFlashMode.setTag("90");
                    }
                    if (!mBackViewer.getTag().equals("90")) {
                        mBackViewer.animate().rotation(-90).start();
                        mBackViewer.setTag("90");
                    }
                }

                if (i > -45 && i < 45) {
                    if (!mFlashMode.getTag().equals("0")) {
                        mFlashMode.animate().rotation(0).start();
                        mFlashMode.setTag("0");
                    }
                    if (!mBackViewer.getTag().equals("0")) {
                        mBackViewer.animate().rotation(0).start();
                        mBackViewer.setTag("0");
                    }
                }

                if (i > 135 && i < 225) {
                    if (!mFlashMode.getTag().equals("180")) {
                        mFlashMode.animate().rotation(180).start();
                        mFlashMode.setTag("180");
                    }
                    if (!mBackViewer.getTag().equals("180")) {
                        mBackViewer.animate().rotation(180).start();
                        mBackViewer.setTag("180");
                    }
                }

                if (i > 225 && i < 315) {
                    if (!mFlashMode.getTag().equals("270")) {
                        mFlashMode.animate().rotation(90).start();
                        mFlashMode.setTag("270");
                    }
                    if (!mBackViewer.getTag().equals("270")) {
                        mBackViewer.animate().rotation(90).start();
                        mBackViewer.setTag("270");
                    }
                }
            }
        };

        if (orientationEventListener.canDetectOrientation()) {
            orientationEventListener.enable();
        }

        mOpenCvCameraView = (Zoomcameraview) findViewById(R.id.HelloOpenCvView);
        mOpenCvCameraView.setZoomControl((SeekBar) findViewById(R.id.CameraZoomControls));

        mBlur = (TextView) findViewById(R.id.blur_indicator);
        if (!checkPermission()) requestPermission();
        else {
            mOpenCvCameraView.setCvCameraViewListener(new CameraBridgeViewBase.CvCameraViewListener2() {
                @Override
                public void onCameraViewStarted(int width, int height) {
                    mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
                    mOpenCvCameraView.setEffect(Camera.Parameters.FOCUS_MODE_AUTO);
                    mOpenCvCameraView.setEffect(Camera.Parameters.FLASH_MODE_AUTO);
                    mOpenCvCameraView.setImage(mPreviewFoto);
                    mOpenCvCameraView.setTouchFocusIndicator(mTouchfocus);
                }

                @Override
                public void onCameraViewStopped() {
                    if (mRgba != null) mRgba.release();
                }

                @Override
                public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
                    mRgba = inputFrame.rgba();

                    Bitmap bm = Bitmap.createBitmap(mRgba.cols(), mRgba.rows(), Bitmap.Config.ARGB_8888);
                    Utils.matToBitmap(mRgba, bm);
                    opencvProcess(bm);

                    return mRgba;
                }
            });
        }

        klien = new AsyncHttpClient();
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    if (checkPermission()) mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
        } else {
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    private void opencvProcess(Bitmap bitmap) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inDither = true;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap image = bitmap;
        int l = CvType.CV_8UC1; //8-bit grey scale image
        Mat matImage = new Mat();
        Utils.bitmapToMat(image, matImage);
        Mat matImageGrey = new Mat();
        Imgproc.cvtColor(matImage, matImageGrey, Imgproc.COLOR_BGR2GRAY);

        Bitmap destImage;
        destImage = Bitmap.createBitmap(image);
        Mat dst2 = new Mat();
        Utils.bitmapToMat(destImage, dst2);
        Mat laplacianImage = new Mat();
        dst2.convertTo(laplacianImage, l);
        Imgproc.Laplacian(matImageGrey, laplacianImage, CvType.CV_8U);
        Mat laplacianImage8bit = new Mat();
        laplacianImage.convertTo(laplacianImage8bit, l);

        Bitmap bmp = Bitmap.createBitmap(laplacianImage8bit.cols(), laplacianImage8bit.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(laplacianImage8bit, bmp);
        int[] pixels = new int[bmp.getHeight() * bmp.getWidth()];
        bmp.getPixels(pixels, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());
        int maxLap = -16777216; // 16m
        for (int pixel : pixels) {
            if (pixel > maxLap)
                maxLap = pixel;
        }

        final int soglia = -6118750;
        if (maxLap <= soglia) {
            final int finalMaxLap = maxLap;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBlur.setText("Blurry " + finalMaxLap);
                }
            });
            Log.d("unik", "blur");
        } else {
            final int finalMaxLap = maxLap;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBlur.setText("Not Blurry " + finalMaxLap);
                }
            });
            Log.d("unik", "not blur");
        }
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                0);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    mOpenCvCameraView.setCvCameraViewListener(new CameraBridgeViewBase.CvCameraViewListener2() {
                        @Override
                        public void onCameraViewStarted(int width, int height) {
                            mOpenCvCameraView.enableView();
                            mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
                            mOpenCvCameraView.setEffect(Camera.Parameters.FOCUS_MODE_AUTO);
                            mOpenCvCameraView.setEffect(Camera.Parameters.FLASH_MODE_AUTO);
                            mOpenCvCameraView.setImage(mPreviewFoto);
                            mOpenCvCameraView.setTouchFocusIndicator(mTouchfocus);
                        }

                        @Override
                        public void onCameraViewStopped() {
                            if (mRgba != null) mRgba.release();
                        }

                        @Override
                        public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
                            mRgba = inputFrame.rgba();

                            Bitmap bm = Bitmap.createBitmap(mRgba.cols(), mRgba.rows(), Bitmap.Config.ARGB_8888);
                            Utils.matToBitmap(mRgba, bm);
                            opencvProcess(bm);

                            return mRgba;
                        }
                    });
                } else {
                    finish();
                }
                break;
        }

    }

    private void initView() {
        mHelloOpenCvView = (Zoomcameraview) findViewById(R.id.HelloOpenCvView);
        mBlurIndicator = (TextView) findViewById(R.id.blur_indicator);
        mShutterButton = (View) findViewById(R.id.shutter_button);
        mShutterButton.setOnClickListener(this);
        mFlashMode = (ImageView) findViewById(R.id.flash_mode);
        mFlashMode.setOnClickListener(this);
        mCameraZoomControls = (SeekBar) findViewById(R.id.CameraZoomControls);
        mPreviewFoto = (ImageView) findViewById(R.id.preview_foto);
        mOverlay = (RelativeLayout) findViewById(R.id.overlay);
        mOverlay.setOnClickListener(this);
        mUpload = (ImageView) findViewById(R.id.upload);
        mUpload.setOnClickListener(this);
        mRetake = (ImageView) findViewById(R.id.retake);
        mRetake.setOnClickListener(this);
        mTouchfocus = (View) findViewById(R.id.touchfocus);
        mBackViewer = (ImageView) findViewById(R.id.back_viewer);
        mBackViewer.setOnClickListener(this);

        mFlashMode.setTag("0");
        mBackViewer.setTag("0");
        mBlurIndicator.setTag("0");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.shutter_button:
                mShutterButton.animate().alpha(0.5f).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        mShutterButton.animate().alpha(1f).setListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {
                                mHelloOpenCvView.takePicture(Integer.parseInt((String) mFlashMode.getTag()));
                                mOverlay.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
                break;
            case R.id.flash_mode:
                if (flashMode.equals("auto")) {
                    flashMode = "off";
                    mOpenCvCameraView.setEffect(Camera.Parameters.FLASH_MODE_OFF);
                    mFlashMode.setImageResource(R.drawable.ic_flash_off_black_24dp);
                } else if (flashMode.equals("off")) {
                    flashMode = "on";
                    mOpenCvCameraView.setEffect(Camera.Parameters.FLASH_MODE_ON);
                    mFlashMode.setImageResource(R.drawable.ic_flash_on_black_24dp);
                } else {
                    flashMode = "auto";
                    mOpenCvCameraView.setEffect(Camera.Parameters.FLASH_MODE_AUTO);
                    mFlashMode.setImageResource(R.drawable.ic_flash_auto_black_24dp);
                }
                break;
            case R.id.overlay:
                break;
            case R.id.upload:
                new encodeBitmapTask(((BitmapDrawable) mPreviewFoto.getDrawable()).getBitmap()).execute();
                break;
            case R.id.retake:
                mOverlay.setVisibility(View.GONE);
                mPreviewFoto.setVisibility(View.GONE);
                break;
            case R.id.back_viewer:
                break;
        }
    }

    public class encodeBitmapTask extends AsyncTask<RequestParams, RequestParams, RequestParams> {
        Bitmap bitmap;
        private ProgressDialog mProgress;


        public encodeBitmapTask(Bitmap bitmap) {
            this.bitmap = bitmap;
        }

        @Override
        protected void onPreExecute() {
            mProgress = new ProgressDialog(MainActivity.this);
            mProgress.setMessage("Menggali emas...");
            mProgress.setCancelable(false);
            mProgress.show();
        }

        @Override
        protected void onPostExecute(RequestParams requestParams) {
            uploadImage(requestParams, mProgress);
        }

        @Override
        protected RequestParams doInBackground(RequestParams... requestParams) {
            RequestParams request = new RequestParams();

            saveImage(bitmap, System.currentTimeMillis() + "");

            if (bitmap != null) {
                byte[] imagebyte;
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, bao);
                imagebyte = bao.toByteArray();
                request.put("myFile", new ByteArrayInputStream(
                        imagebyte), "Image" + System.currentTimeMillis()
                        + ".jpg");
            }

            return request;
        }
    }

    public void uploadImage(RequestParams request, final ProgressDialog mProgress) {
        final

        AsyncHttpClient client = new AsyncHttpClient();

        client.post("http://geco.transparansi.xyz/savetofile.php", request, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
                Toast.makeText(MainActivity.this, "Sukses", Toast.LENGTH_LONG).show();
                mProgress.dismiss();
            }

            @Override
            public void onFailure(int arg0, Header[] arg1, byte[] arg2,
                                  Throwable arg3) {
                Toast.makeText(MainActivity.this, "Gagal", Toast.LENGTH_LONG).show();
                mProgress.dismiss();
            }
        });
    }

    private void saveImage(Bitmap finalBitmap, String image_name) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root);
        myDir.mkdirs();
        String fname = "Image-" + image_name + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();

        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}