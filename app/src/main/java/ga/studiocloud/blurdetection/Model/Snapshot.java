package ga.studiocloud.blurdetection.Model;

import android.hardware.Camera;

public class Snapshot {
    byte[] data;
    Camera camera;

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public Snapshot(byte[] data, Camera camera) {
        this.data = data;
        this.camera = camera;
    }
}
