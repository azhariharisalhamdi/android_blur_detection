package org.opencv.android;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

import org.opencv.android.JavaCameraView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Zoomcameraview extends JavaCameraView implements Camera.PictureCallback,Camera.AutoFocusCallback{
    private String mPictureFileName;
    Context konteks;
    ImageView imageView;
    private static  final int FOCUS_AREA_SIZE= 300;
    private View touchFocusIndicator;
    private int mPictureAngle = 0;

    public Zoomcameraview(Context context, int cameraId) {
        super(context, cameraId);
        konteks = context;
        this.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    focusOnTouch(event);
                    touchFocusIndicator.setX(event.getX()-(touchFocusIndicator.getWidth()/2.0f));
                    touchFocusIndicator.setY(event.getY()-(touchFocusIndicator.getHeight()/2.0f));
                    touchFocusIndicator.setAlpha(0);
                    touchFocusIndicator.setVisibility(View.VISIBLE);
                    touchFocusIndicator.animate().setDuration(1000).setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            touchFocusIndicator.animate().setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {
                                    touchFocusIndicator.setVisibility(GONE);
                                    touchFocusIndicator.setScaleX(1f);
                                    touchFocusIndicator.setScaleY(1f);
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {

                                }
                            }).alpha(0).start();
                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    }).alpha(1).start();
                    touchFocusIndicator.animate().setDuration(1000).scaleXBy(0.3f).start();
                    touchFocusIndicator.animate().setDuration(1000).scaleYBy(0.3f).start();
                }
                return true;
            }
        });
    }

    private void focusOnTouch(MotionEvent event) {
        if (mCamera != null ) {

            Camera.Parameters parameters = mCamera.getParameters();
            if (parameters.getMaxNumMeteringAreas() > 0){
                Rect rect = calculateFocusArea(event.getX(), event.getY());

                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                List<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();
                meteringAreas.add(new Camera.Area(rect, 800));
                parameters.setFocusAreas(meteringAreas);

                mCamera.setParameters(parameters);
                mCamera.autoFocus(this);
            }else {
                mCamera.autoFocus(this);
            }
        }
    }

    private Rect calculateFocusArea(float x, float y) {
        int left = clamp(Float.valueOf((x / this.getWidth()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);
        int top = clamp(Float.valueOf((y / this.getHeight()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);

        return new Rect(left, top, left + FOCUS_AREA_SIZE, top + FOCUS_AREA_SIZE);
    }

    private int clamp(int touchCoordinateInCameraReper, int focusAreaSize) {
        int result;
        if (Math.abs(touchCoordinateInCameraReper)+focusAreaSize/2>1000){
            if (touchCoordinateInCameraReper>0){
                result = 1000 - focusAreaSize/2;
            } else {
                result = -1000 + focusAreaSize/2;
            }
        } else{
            result = touchCoordinateInCameraReper - focusAreaSize/2;
        }
        return result;
    }

    public Zoomcameraview(Context context, AttributeSet attrs) {
        super(context, attrs);
        konteks = context;
        this.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    focusOnTouch(event);
                    touchFocusIndicator.setX(event.getX()-(touchFocusIndicator.getWidth()/2.0f));
                    touchFocusIndicator.setY(event.getY()-(touchFocusIndicator.getHeight()/2.0f));

                    touchFocusIndicator.setAlpha(0);
                    touchFocusIndicator.setVisibility(View.VISIBLE);
                    touchFocusIndicator.animate().setDuration(1000).setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            touchFocusIndicator.animate().setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {
                                    touchFocusIndicator.setVisibility(GONE);
                                    touchFocusIndicator.setScaleX(1f);
                                    touchFocusIndicator.setScaleY(1f);
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {

                                }
                            }).alpha(0).start();
                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    }).alpha(1).start();
                    touchFocusIndicator.animate().setDuration(1000).scaleXBy(0.3f).start();
                    touchFocusIndicator.animate().setDuration(1000).scaleYBy(0.3f).start();
                }
                return true;
            }
        });
    }

    public void setImage(ImageView imageView)
    {
        this.imageView = imageView;
    }

    protected SeekBar seekBar;

    public void takePicture(int angle) {
        this.mPictureAngle = angle;
        // Postview and jpeg are sent in the same buffers if the queue is not empty when performing a capture.
        // Clear up buffers to avoid mCamera.takePicture to be stuck because of a memory issue
        mCamera.setPreviewCallback(null);

        // PictureCallback is implemented by the current class
        mCamera.takePicture(null, null, this);
    }

    public List<String> getEffectList() {
        return mCamera.getParameters().getSupportedFlashModes();
    }
    public boolean isEffectSupported() {
        return (mCamera.getParameters().getFlashMode() != null);
    }
    public String getEffect() {
        return mCamera.getParameters().getFlashMode();
    }
    public void setEffect(String effect) {
        mCamera.getParameters();
        Camera.Parameters params = mCamera.getParameters();
        params.setFlashMode(effect);
        mCamera.setParameters(params);
    }
    public List<Camera.Size> getResolutionList() {
        return mCamera.getParameters().getSupportedPreviewSizes();
    }
    public void setResolution(int w, int h) {
        disconnectCamera();
        mMaxHeight = h;
        mMaxWidth = w;
        connectCamera(getWidth(), getHeight());
    }

    public Camera.Size getResolution() {
        return mCamera.getParameters().getPreviewSize();
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        // The camera preview was automatically stopped. Start it again.
        mCamera.startPreview();
        mCamera.setPreviewCallback(this);

        Camera.Parameters parameters = camera.getParameters();
        int width = parameters.getPreviewSize().width;
        int height = parameters.getPreviewSize().height;

        Bitmap bitmap = BitmapFactory.decodeByteArray(data , 0, data.length);

        Matrix matrix = new Matrix();

        matrix.postRotate(mPictureAngle+90);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
        final Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        ((Activity)konteks).runOnUiThread(new Runnable() {

            @Override
            public void run() {
                imageView.setImageBitmap(rotatedBitmap);
                imageView.setVisibility(View.VISIBLE);
            }
        });
    }

    public Bitmap decodeFile(File f) {
        Bitmap b = null;
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            FileInputStream fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();
            int IMAGE_MAX_SIZE = 1000;
            int scale = 1;
            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int) Math.pow(
                        2,
                        (int) Math.round(Math.log(IMAGE_MAX_SIZE
                                / (double) Math.max(o.outHeight, o.outWidth))
                                / Math.log(0.5)));
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return b;
    }

    public void cameraRelease() {
        if(mCamera != null){
            mCamera.release();
        }
    }

    public void setZoomControl(SeekBar _seekBar) {
        seekBar = _seekBar;
    }

    protected void enableZoomControls(Camera.Parameters params) {

        final int maxZoom = params.getMaxZoom();
        seekBar.setMax(maxZoom);
        seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                                               int progressvalue = 0;

                                               @Override
                                               public void onProgressChanged(SeekBar seekBar, int progress,
                                                                             boolean fromUser) {
                                                   // TODO Auto-generated method stub
                                                   progressvalue = progress;
                                                   Camera.Parameters params = mCamera.getParameters();
                                                   params.setZoom(progress);
                                                   mCamera.setParameters(params);


                                               }

                                               @Override
                                               public void onStartTrackingTouch(SeekBar seekBar) {
                                                   // TODO Auto-generated method stub

                                               }

                                               @Override
                                               public void onStopTrackingTouch(SeekBar seekBar) {
                                                   // TODO Auto-generated method stub

                                               }


                                           }

        );

    }


    protected boolean initializeCamera(int width, int height) {

        boolean ret = super.initializeCamera(width, height);

        if (mCamera == null)
            return false;

        try {
            Camera.Parameters params = mCamera.getParameters();

            if (params.isZoomSupported())
                enableZoomControls(params);

            mCamera.setParameters(params);
        } catch (Exception e){}

        return ret;
    }

    @Override
    public void onAutoFocus(boolean b, Camera camera) {
        if (b) {
            // do something...
            Log.i("tap_to_focus","success!");
        } else {
            // do something...
            Log.i("tap_to_focus","fail!");
        }
    }

    public void setTouchFocusIndicator(View touchFocusIndicator) {
        this.touchFocusIndicator = touchFocusIndicator;
    }
}